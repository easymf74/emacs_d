;Emacs Packagemanager
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) 

;use-package für einfache Paketkonfiguration
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(put 'narrow-to-region 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(sanityinc-tomorrow-night))
 '(custom-safe-themes
   '("6fc9e40b4375d9d8d0d9521505849ab4d04220ed470db0b78b700230da0a86c1" "c38363d290dc139853ae018ec595b5fa477769d828f350c4f93708e9591ba5de" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" default))
 '(org-agenda-files
   '("/home/maik/Dokumente/workspace/wiki/daily/2022-09-30.org" "/home/maik/Dokumente/workspace/wiki/daily/2022-09-25.org"))
 '(package-selected-packages
   '(diff-hl denote dnote minions firecode-theme perspective lua-mode popper evil yasnippet company treemacs ivy lsp-ivy lsp-ui ccls flycheck treemacs-projectile org-roam move-dup treemacs-magit treemacs-icons-dired engine-mode base16-theme which-key use-package swiper smex ox-reveal org-tree-slide org-bullets monokai-theme magit leuven-theme hydra htmlize expand-region dumb-jump deft dashboard company-c-headers color-theme-sanityinc-tomorrow badger-theme auto-yasnippet arc-dark-theme ag ace-window))
 '(persp-mode-prefix-key [24 121]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

; führe myemacs.org aus
(org-babel-load-file (expand-file-name "~/.emacs.d/myemacs.org"))
(put 'upcase-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
