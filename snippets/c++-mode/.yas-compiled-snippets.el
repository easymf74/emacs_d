;;; Compiled snippets and support files for `c++-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'c++-mode
		     '(("while" "while(${1:Bedingung}){\n	$2\n\n}" "while" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/while" nil nil)
		       ("try" "try{\n	$1\n}catch( ${2:FEHELERTYP} ${3:e} ){\n	$4\n}" "try" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/try" nil nil)
		       ("switch" "switch(${1:VARIABLE}){\n	case ${2:WERT1}:\n	     $3\n	     break;\n	case ${4:WERT2}:\n	     $5\n	     break;\n	${6:default:}	\n		$7\n}\n$0" "switch" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/switch" nil nil)
		       ("ptr" "std::unique_ptr<${1:TYP}> $2\n" "ptr" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/ptr" nil nil)
		       ("main" "int main(int argc, char* argv[]){\n    $0\n    return 0;\n}\n" "main" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/main" nil nil)
		       ("it" "for (auto i = ${1:Container}.begin(); i != $1.end(); ++i){\n    $0for (auto i= Test.begin(); i != Test.end(); ++i){\n    \n  }\n\n}\n\n\n" "it" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/it" nil nil)
		       ("io" "#include <iostream>" "iostream" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/iostream" nil nil)
		       ("incl" "#include ${1:\"`(file-name-nondirectory (file-name-sans-extension (buffer-file-name)))`.h\"}\n$0" "incl" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/include" nil nil)
		       ("#+" "#include \"$1\"\n$0" "include personal headers" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/inclmy" nil nil)
		       ("#<" "#include<`(header-to-include)`>\n$0" "include std-header" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/incl_std" nil nil)
		       ("ifstream" "std::ifstream ${1:file} (${2:path});\n\nif($1.is_open()){\n  std::string ${3:line};\n  while ( getline ($1,$3) )\n  {\n	$0   \n  }\n  file.close();\n}else{\n  std::cerr << \"Datei \" << $2 << \" konnte nicht geöffnet werden\" << std::endl;\n} " "ifstream" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/ifstream" nil nil)
		       ("ifndef" "#ifndef ${1:`(upcase (file-name-nondirectory (file-name-sans-extension (buffer-file-name))))`_H}\n#define $1  $1\n\n$0\n\n#endif //$1\n\n" "ifndef" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/ifndef" nil nil)
		       ("if" "if (${1:Bedingung}){\n   $2\n}$0" "if" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/if" nil nil)
		       ("fore" "for(${1:const} ${2:int}${3:&} ${4:item} : ${5:container}){\n  $6\n}\n$0" "for eache" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/foreach" nil nil)
		       ("for" "for (${1:int} ${2:i}${3:=0}; $2 ${4:<} ${5:Durchläufe}; ${6:++}$2){\n$7\n}\n$0\n" "for" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/for" nil nil)
		       ("endl" "`(progn (goto-char (point-min)) (unless (re-search-forward\n\"^using\\\\s-+namespace std;\" nil 'no-error) \"std::\"))\n`endl" "endl" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/endl" nil nil)
		       ("eli" "else if(${1:Bedingung}){\n$2\n}$0\n" "else if" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/elseif" nil nil)
		       ("el" "else{\n	$1\n}\n$0\n" "else" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/else" nil nil)
		       ("cd" "// ${1:`(file-name-nondirectory(buffer-file-name))`}" "datei" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/dateiname" nil nil)
		       ("cout" "`(progn (goto-char (point-min)) (unless (re-search-forward\n\"^using\\\\s-+namespace std;\" nil 'no-error) \"std::\"))\n`cout <<" "cout" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/cout" nil nil)
		       ("/*" "/** @short $1\n\n    $0\n */" "Dokumentations Kommentar" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/comment" nil nil)
		       ("class" "class ${1:Classname} $2{\n$0\n};" "class" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/class" nil nil)
		       ("cin" "`(progn (goto-char (point-min)) (unless (re-search-forward\n\"^using\\\\s-+namespace std;\" nil 'no-error) \"std::\"))\n`cin >>" "cin" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/cin" nil nil)
		       ("case" "case ${1:Wert}:\n     $2\n     break;" "case" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/case" nil nil)
		       ("make" "#Makefile _generic_\n## Quelle: https://latedev.wordpress.com/2014/11/08/generic-makefiles-with-gcc-and-gnu-make/\nPRGNAME=program\n\nCOMPILER=g++\nLINKER=g++\nCF=-std=c++11 -Wall -pedantic\nINCDIRS=-I.\n\n#generate a list of all files which end with the .cpp extension\nSRC=$(wildcard *.cpp)\n#generate a list of object-files by repalace .cpp trough .o\nOBJ=$(patsubst %.cpp,%.o,$(SRC))\n#generate a list of object-files by repalace .cpp trough .d\n#to write later the output of g++ -MM %.cpp in the file\nDEP=$(patsubst %.cpp,%.d,$(SRC))\nLIBS=\n\n$(PRGNAME): $(OBJ)\n	$(LINKER) -o $@ $^ $(LIBS)\n\n%.o: %.cpp\n	$(COMPILER) $(CF) $(INCDIRS) -c $< -o $@\n\n#to write the output of 'g++ -MM %.cpp' in the d-file\n#corespondending to the %.cpp-file	\n%.d: %.cpp\n	$(COMPILER) $(INCDIRS) -MM $< > $@\n\n# include the d-files, where the output from\n# 'g++ -MM %.cpp' is\n# and let make use his implecit rules to\n# build a o-file from a cpp-file	\n-include $(DEP)\n\n\nclean:\n	$(RM) *.o *.d\n" "Makefile" nil nil nil "/home/maik/.emacs.d/snippets/c++-mode/Makefile" nil nil)))


;;; Do not edit! File generated at Sat Aug 17 05:48:37 2024
