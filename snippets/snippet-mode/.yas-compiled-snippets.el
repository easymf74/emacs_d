;;; Compiled snippets and support files for `snippet-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'snippet-mode
		     '(("ifndef" "#ifndef ${1:`(upcase (file-name-nondirectory (file-name-sans-extension (buffer-file-name))))`_H}\n#define $1_H $1_H\n\n$0\n\n#endif //$1_H" "ifndef" nil nil nil "/home/maik/.emacs.d/snippets/snippet-mode/+new-snippet+" nil nil)))


;;; Do not edit! File generated at Sat Aug 17 05:48:37 2024
